// Simple Calculator
// Sawyer Grambihler

#include <iostream>
#include <conio.h>

using namespace std;

void GetNumber(float &number);
float Add(float, float);
float Subtract(float, float);
float Multiply(float, float);
bool Divide(float, float, float &anser);
float Pow(float, int);

int main() {
	float num1, num2, result;
	char operation, loop = 'Y';
	bool error;

	// If the user keeps entering Y keep running
	while (loop == 'Y')
	{
		error = false;
		//Call the function to get a number from the user
		GetNumber(num1);

		//Ask the user what operation
		cout << "Please select an operation\n\"+\" for addition\n\"-\" for subtraction\n\"*\" for multiplication\n\"/\" for division\n\"^\" for Exponents\n";
		cin >> operation;

		GetNumber(num2);

		//Call a math function depending on which operation the user selected
		switch (operation)
		{
		case '+':
			result = Add(num1, num2);
			break;

		case '-':
			result = Subtract(num1, num2);
			break;

		case '*':
			result = Multiply(num1, num2);
			break;

		case '^':
			result = Pow(num1, (int)num2);
			break;

		case '/':
			if (!Divide(num1, num2, result)) {
				cout << "Can not didvide by 0";
				error = true;
				(void)_getch();
			}
			break;

		default:
			cout << "That is not a valid operation";
			error = true;
			(void)_getch();
		}

		//If there were no errors show the user the result
		if(!error)
		cout << num1 << " " << operation << " " << num2 << " = " << result;

		//Ask the user if they want to go again
		cout << "\nEnter \"Y\" to continue or anything else to exit\n";
		cin >> loop;
		loop = toupper(loop);
	}
}

void GetNumber(float &number) {
	do {
		cout << "Please enter a positive number\n";
		cin >> number;
	} while (number < 0);
}

float Add(float num1, float num2) {
	return num1 + num2;
}

float Subtract(float num1, float num2) {
	return num1 - num2;
}

float Multiply(float num1, float num2) {
	return num1 * num2;
}

bool Divide(float num1, float num2, float &answer) {
	if (num2 == 0)
		return false;
	else {
		answer = num1 / num2;
		return true;
	}
}

float Pow(float num1, int num2)
{
	if (num2 == 0) {
		return 1;
	}
	else
	{
		return num1 * Pow(num1, num2 - 1);
	}
}